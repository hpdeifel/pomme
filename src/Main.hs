{-# LANGUAGE OverloadedStrings #-}

module Main where

import           Control.Applicative            ( (<**>) )
import           Control.Monad                  ( void )

import qualified DBus                          as DBus
import qualified DBus.Client                   as DBus
import qualified Options.Applicative           as Opt

data Command = Start | Stop | Pause | Resume
  deriving (Show)

commandParser :: Opt.Parser Command
commandParser =
  Opt.hsubparser
    $  Opt.command "start" (Opt.info (pure Start) Opt.idm)
    <> Opt.command "stop" (Opt.info (pure Stop) Opt.idm)
    <> Opt.command "pause" (Opt.info (pure Pause) Opt.idm)
    <> Opt.command "resume" (Opt.info (pure Resume) Opt.idm)

optParser :: Opt.Parser Command
optParser = Opt.hsubparser (Opt.command "de" (Opt.info commandParser Opt.idm))

callMethod :: DBus.MemberName -> IO ()
callMethod method = do
  client <- DBus.connectSession
  void $ DBus.call_
    client
    (DBus.methodCall "/org/gnome/Pomodoro" "org.gnome.Pomodoro" method)
      { DBus.methodCallDestination = Just "org.gnome.Pomodoro"
      }


main :: IO ()
main = do
  let desc = Opt.fullDesc <> Opt.progDesc "Control the GNOME pomodoro timer"
  command <- Opt.execParser (Opt.info (optParser <**> Opt.helper) desc)
  callMethod $ case command of
    Start  -> "Start"
    Stop   -> "Stop"
    Pause  -> "Pause"
    Resume -> "Resume"
