# pomme

A command line client for the GNOME shell pomodoro extension.

## Usage

Start the pomodoro timer:

```
pomme de start
```

Stop the pomodoro timer:

```
pomme de stop
```

Pause the pomodoro timer:

```
pomme de pause
```

Resume the pomodoro timer:

```
pomme de resume
```

## Installation

```
cabal new-install .
```
